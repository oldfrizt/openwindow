import weatherData.dateSet;

/**
 * Time and Date are separated and returned as a dateSet
 *
 * @author Vincent Wagner
 * @version 13.06.2019
 */
class format {

    /**
     * Constructor
     */
    public format(){}

    /**
     *
     * Takes time and date as a String and returns a dateSet containing them split up.
     *
     * @param s Time an date inf following format as String: "YYYY-MM-DDTHH:MM"
     * @return dateSet containing information of time and date string
     */
    public dateSet formatDate(String s){

        dateSet dateSet = new dateSet();              //new dateSet

        String[] tempDate = s.split("T");       // Split in date an time parts

        String[] date = tempDate[0].split("-"); //Split date part in Year, Month and Day
        dateSet.setYear(Integer.parseInt(date[0]));   //Get Year
        dateSet.setMonth(Integer.parseInt(date[1]));  //Get Month
        dateSet.setDay(Integer.parseInt(date[2]));    //Get Day

        String[] time = tempDate[1].split(":"); //Split time part in Hour, Minute,...
        dateSet.setHour(Integer.parseInt(time[0]));   //Get Hour
        dateSet.setMinute(Integer.parseInt(time[1])); //Get Minute

        return dateSet;                               //return complete dateSet
    }
}
