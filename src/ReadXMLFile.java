import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import weatherData.weatherData;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.Objects;

/**
 * This class is used to Read and analyse the XML File with the weather forecast information
 *
 * @author Vicent Wagner
 * @version 13.06.2019
 */
class ReadXMLFile {

    private final format f;
    private final weatherData weatherData;

    public ReadXMLFile(File file, weatherData weatherData) {

        this.weatherData = weatherData;

        f = new format();

        try {

            DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

            Document doc = dBuilder.parse(file);

            if(main.DEBUG) System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

            if (doc.hasChildNodes()) {

                //printNote(doc.getChildNodes());
                print(doc.getChildNodes());

            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    private void print(NodeList nodeList){

        for(int i = 0; i < nodeList.getLength(); i++){
            Node tempNode = nodeList.item(i);
            if (tempNode.getNodeType() == Node.ELEMENT_NODE){
                if(Objects.equals(tempNode.getNodeName(), "time")){
                    if(main.DEBUG) System.out.println("time");
                    NamedNodeMap noteMap = tempNode.getAttributes();
                    String from = noteMap.item(0).getNodeValue();
                    String to = noteMap.item(1).getNodeValue();
                    weatherData.addWeatherSet(i, f.formatDate(from),getTemp(tempNode), getRain(tempNode));
                    if(main.DEBUG) System.out.println("done " + i);
                }
                if (tempNode.hasChildNodes()){
                    print(tempNode.getChildNodes());
                }
            }
        }

    }

    private int getTemp(Node timeNode) {

        double intTemp = -1;
        NodeList nodeList = timeNode.getChildNodes();

        for(int i = 0; i < nodeList.getLength(); i++){
            Node tempNode = nodeList.item(i);
            if (tempNode.getNodeType() == Node.ELEMENT_NODE){
                if(Objects.equals(tempNode.getNodeName(), "temperature")){
                    if(main.DEBUG) System.out.println("temp");
                    NamedNodeMap noteMap = tempNode.getAttributes();
                    String temp = noteMap.item(1).getNodeValue();
                    intTemp = Double.parseDouble(temp);
                    }
            }
        }

        return (int) intTemp;
    }

    private float getRain(Node timeNode) {

        float fRain = 0;
        NodeList nodeList = timeNode.getChildNodes();

        for(int i = 0; i < nodeList.getLength(); i++){
            Node tempNode = nodeList.item(i);
            if (tempNode.getNodeType() == Node.ELEMENT_NODE){
                if(Objects.equals(tempNode.getNodeName(), "precipitation")){
                    if(main.DEBUG) System.out.println("rain");
                    NamedNodeMap noteMap = tempNode.getAttributes();
                    if(tempNode.hasAttributes()) {
                        String rain = noteMap.item(2).getNodeValue();
                        fRain = Float.parseFloat(rain);
                    }
                }
            }
        }

        return fRain;
    }

    private static void printNote(NodeList nodeList) {

        for (int count = 0; count < nodeList.getLength(); count++) {

            Node tempNode = nodeList.item(count);

            // make sure it's element node.
            if (tempNode.getNodeType() == Node.ELEMENT_NODE) {

                // get node name and value
                if(main.DEBUG) System.out.println("\nNode Name =" + tempNode.getNodeName() + " [OPEN]");
                if(main.DEBUG) System.out.println("Node Value =" + tempNode.getTextContent());

                if (tempNode.hasAttributes()) {

                    // get attributes names and values
                    NamedNodeMap nodeMap = tempNode.getAttributes();

                    for (int i = 0; i < nodeMap.getLength(); i++) {

                        Node node = nodeMap.item(i);
                        if(main.DEBUG) System.out.println("attr name : " + node.getNodeName());
                        if(main.DEBUG) System.out.println("attr value : " + node.getNodeValue());

                    }

                }

                if (tempNode.hasChildNodes()) {

                    // loop again if has child nodes
                    printNote(tempNode.getChildNodes());

                }

                if(main.DEBUG) System.out.println("Node Name =" + tempNode.getNodeName() + " [CLOSE]");

            }

        }

    }

}