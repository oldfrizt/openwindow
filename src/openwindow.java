import timeData.timeData;
import weatherData.weatherData;

import java.time.LocalTime;

public class openwindow {

    private weatherData weatherData;
    private timeData timeData;

    public openwindow(){

        getWeatherData();
        getCurrentTime();

    }

    private void getWeatherData(){
        weatherCheck weatherCheck = new weatherCheck();
        weatherData = weatherCheck.getWeatherData();
    }

    private void getCurrentTime(){
        timeData = new timeData();
        timeData.setHour(LocalTime.now().getHour());
        timeData.setMinute(LocalTime.now().getMinute());
        if(main.DEBUG) System.out.println("Hour: " + timeData.getHour());
        if(main.DEBUG) System.out.println("Minute: " + timeData.getMinute());
    }

}
