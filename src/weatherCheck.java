import weatherData.weatherData;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * This class initializes the Weather check
 *
 * @author Vincent Wagner
 * @version 13.06.2019
 */
class weatherCheck {

    private final weatherData weatherData;


    public weatherCheck(){

        weatherData = new weatherData(main.DEBUG);
        connect();
    }

    /**
     * Method to connect to openweathermap api and get the Data in xml format
     */
    private void connect(){
        try{
            URL url = new URL("https://api.openweathermap.org/data/2.5/forecast?q=Augsburg,de&mode=xml&units=metric&appid=a1c494f208acb991fc907c7c1aa471c5"); //URL used for fetching weather data
            File file = new File("weather.xml"); //Create File used for storing web output
            file.deleteOnExit();    //Delete file on Exit
            try (PrintStream out = new PrintStream(new FileOutputStream(file))) {
                out.print(show(url));   //Print String to file
            }
            ReadXMLFile readXMLFile = new ReadXMLFile(file, weatherData); //Read and analyse XML file. weatherData object is filled
        }catch (MalformedURLException e){
            System.out.println(e.toString());
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * URL is downloaded and returned as a String
     *
     * @param url The URL used for fetching data
     * @return Data returned from web browser
     * @throws IOException connection problems
     */
    private String show(URL url) throws IOException {
        InputStream in = url.openStream();
        BufferedReader buff = new BufferedReader(new InputStreamReader(in));
        StringBuilder s = new StringBuilder();
        String n;
        while((n = buff.readLine()) != null){
            s.append(n);
        }
        return s.toString();
    }

    public weatherData getWeatherData(){
        return weatherData;
    }

}
