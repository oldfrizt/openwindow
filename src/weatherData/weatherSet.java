package weatherData;

/**
 * Contains the information of the weather at a certain time
 *
 * @author Vincent Wagner
 * @version 13.06.2019
 */
class weatherSet {

    //Variables containing weather Information
    private dateSet date;
    private int temperature;
    private float rainValue;

    /**
     * Constructor
     */
    public weatherSet() {

    }


    //Getter and Setter Methods for dateSet, temperature and rainValue

    public dateSet getDate() {
        return date;
    }

    public void setDate(dateSet date) {
        this.date = date;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public float getRainValue() {
        return rainValue;
    }

    public void setRainValue(float rainValue) {
        this.rainValue = rainValue;
    }

}
