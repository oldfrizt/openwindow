package weatherData;

/**
 * contains all the weather sets which contain information about time and weather
 *
 * @author Vincent Wagner
 * @version 13.06.2019
 */
public class weatherData {

    private final weatherSet[] sets; //Contains all the weatherSets, read from the web server

    private static boolean DEBUG;

    /**
     * Constructor.
     */
    public weatherData(boolean DEBUG){

        this.DEBUG = DEBUG;

        sets = new weatherSet[40];
        for (int i = 0; i < 40; i++) {
            sets[i] = new weatherSet(); // Initialisation of weatherSet array
        }

    }

    /**
     *
     * adds the weather information to the weatherSet at a certain index
     *
     * @param index Index in weatherSet array
     * @param date dateSet containing date informartion
     * @param temperature temperature at current dateSet
     * @param rainValue rainValue at current date set. 0 -> no rain
     */
    public void addWeatherSet(int index, dateSet date, int temperature, float rainValue) {

        if(DEBUG) System.out.println("addWeatherSet:");
        if(DEBUG) System.out.println("index: " + index);
        if(DEBUG) System.out.println("Temp: " + temperature);
        if(DEBUG) System.out.println("Rain: " + rainValue);
        if(DEBUG) System.out.println("Time: " + date.getHour());

        sets[index].setDate(date);
        sets[index].setTemperature(temperature);
        sets[index].setRainValue(rainValue);


    }

}

