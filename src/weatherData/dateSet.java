package weatherData;

/**
 * bundles time and date information
 *
 * @author Vincent Wagner
 * @version 13.06.2019
 */
public class dateSet {

    //Time and date containing variables
    private int Year;
    private int Month;
    private int Day;
    private int Hour;
    private int Minute;

    /**
     * Constructor
     */
    public dateSet() {

    }


    //Getter and Setter Methods for Year, Month, Day, Hour, Minute

    public int getYear() {

        return Year;
    }

    public void setYear(int year) {
        Year = year;
    }

    public int getMonth() {
        return Month;
    }

    public void setMonth(int month) {
        Month = month;
    }

    public int getDay() {
        return Day;
    }

    public void setDay(int day) {
        Day = day;
    }

    public int getHour() {
        return Hour;
    }

    public void setHour(int hour) {
        Hour = hour;
    }

    public int getMinute() {
        return Minute;
    }

    public void setMinute(int minute) {
        Minute = minute;
    }
}
